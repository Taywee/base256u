#![no_std]

#[cfg(test)]
extern crate alloc;

/// Encoder iterator, converting bytes into unicode chars based on the contained encoding callable.
pub struct Encoder<I, F>
where
    I: Iterator<Item = u8>,
    F: Fn(u8) -> char,
{
    iterator: I,
    encode: F,
}

impl<I, F> Encoder<I, F>
where
    I: Iterator<Item = u8>,
    F: Fn(u8) -> char,
{
    pub fn new(iterator: I, encode: F) -> Self {
        Self { iterator, encode }
    }
}

impl<I, F> Iterator for Encoder<I, F>
where
    I: Iterator<Item = u8>,
    F: Fn(u8) -> char,
{
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next().map(|byte| (self.encode)(byte))
    }
}

/// Encoder iterator, converting unicode chars into bytes based on the contained decoding callable.
/// Must be fallible because not all unicode codepoints are valid bytes.
pub struct Decoder<I, F>
where
    I: Iterator<Item = char>,
    F: Fn(char) -> Option<u8>,
{
    iterator: I,
    decode: F,
}

impl<I, F> Decoder<I, F>
where
    I: Iterator<Item = char>,
    F: Fn(char) -> Option<u8>,
{
    pub fn new(iterator: I, decode: F) -> Self {
        Self { iterator, decode }
    }
}

impl<I, F> Iterator for Decoder<I, F>
where
    I: Iterator<Item = char>,
    F: Fn(char) -> Option<u8>,
{
    type Item = Option<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        self.iterator.next().map(|byte| (self.decode)(byte))
    }
}

/// Encode function for encoding to printable-ascii-preserving Unicode.
/// `0x00..=0x1F` is mapped to the range starting at `0xB0` to map into the Latin-1 Supplement block in the first range that is 8-byte aligned and fully printable, skipping `NBSP` and `SHY`.
/// `0x20..=0x7E` are mapped to the same bytes as printable ASCII.
/// `0x7F` is arbitrarily mapped from ASCII ESC to §.
/// `0x80..=0xFF`  mapped to the range starting at `0x100`, Latin Extended-A, with the exception of
/// `0xC9`, which is mapped arbitrarily to `¤` to avoid the deprecated character at that codepoint.
pub fn encode_papu(byte: u8) -> char {
    match byte {
        b @ 0x00..=0x1F => (b + 0xB0) as char,
        b @ 0x20..=0x7E => b as char,
        0x7F => '§',
        // Unsafe is fine here because these ranges are known to be safe char values.
        b @ (0x80..=0xC8 | 0xCA..) => unsafe { char::from_u32_unchecked(b as u32 + 0x80) },
        0xC9 => '¤',
    }
}

/// Decode function for printable-ascii-preserving Unicode.
/// All values are mapped as the inverse of encode_papu.  All other input chars map to None.
pub const fn decode_papu(c: char) -> Option<u8> {
    match c as u32 {
        b @ 0xB0..=0xCF => Some((b - 0xB0) as u8),
        b @ 0x20..=0x7E => Some(b as u8),
        0xA7 => Some(0x7F),
        b @ (0x100..=0x148 | 0x14A..=0x17F) => Some((b - 0x80) as u8),
        0xA4 => Some(0xC9),
        _ => None,
    }
}

pub trait Encode: Iterator<Item = u8>
where
    Self: Sized,
{
    fn base256u<F>(self, function: F) -> Encoder<Self, F>
    where
        F: Fn(u8) -> char,
    {
        Encoder::new(self, function)
    }

    fn base256u_papu(self) -> Encoder<Self, fn(u8) -> char> {
        self.base256u(encode_papu)
    }
}

impl<T> Encode for T where T: Iterator<Item = u8> {}

pub trait Decode: Iterator<Item = char>
where
    Self: Sized,
{
    fn base256u<F>(self, function: F) -> Decoder<Self, F>
    where
        F: Fn(char) -> Option<u8>,
    {
        Decoder::new(self, function)
    }

    fn base256u_papu(self) -> Decoder<Self, fn(char) -> Option<u8>> {
        self.base256u(decode_papu)
    }
}

impl<T> Decode for T where T: Iterator<Item = char> {}

#[cfg(test)]
mod tests {
    use crate::{Decode, Encode};
    use alloc::string::String;
    use alloc::vec::Vec;

    #[test]
    fn encoding() {
        let encoded: String = (u8::MIN..=u8::MAX).base256u_papu().collect();
        assert_eq!(encoded, "°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~§ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇň¤ŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ");
        let encoded: String = b"Pack my box with five dozen liquor jugs."
            .into_iter()
            .copied()
            .base256u_papu()
            .collect();
        assert_eq!(encoded, "Pack my box with five dozen liquor jugs.");
    }

    #[test]
    fn decoding() {
        let decoded: Vec<Option<u8>> = "°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏ !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~§ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇň¤ŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſƝŉ".chars().base256u_papu().collect();
        let mut matcher: Vec<Option<u8>> = (u8::MIN..=u8::MAX).map(|b| Some(b)).collect();
        matcher.push(None);
        matcher.push(None);
        assert_eq!(decoded, matcher);
        let decoded: Vec<u8> = "Pack my box with five dozen liquor jugs."
            .chars()
            .base256u_papu()
            .map(|c| c.unwrap())
            .collect();
        assert_eq!(
            String::from_utf8(decoded).unwrap(),
            "Pack my box with five dozen liquor jugs."
        );
    }
}
